<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'jhon' );

/** MySQL database password */
define( 'DB_PASSWORD', 'andrea' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`@axOh+3V9H20fFy.Fg`ZR{QTxI7Ulea.4RBL?aw~5o_&YNYasM !tr#5/aj/FLS' );
define( 'SECURE_AUTH_KEY',  '8K sw))v:W`[:!II@qyk rBKGc-IH9-uM#xWEBo445GXm~b-y4V6@W_|6IrX/`;.' );
define( 'LOGGED_IN_KEY',    'MuLoI@@@*cx!NEv`zx5i&TcLq7d>VX/asvBvSW^ZG=O?-@z[vSfoMi{4sz3<$C`<' );
define( 'NONCE_KEY',        '.YNaqZqsw-&V4t{4 umD]NJz$.FV?U;xlW~ffg +0lTKoJ3htSV2Fbaapi2qxh6E' );
define( 'AUTH_SALT',        '3D{y@+|#+*z5-NMqK@*J.sls1A}E}/^aEXx;,`sP i~B*z2~ZfKt]Hae0*PwIaD*' );
define( 'SECURE_AUTH_SALT', '8Gg-9:mTv>:=ZBAA37,pVYVGKpn[U:$4N=)-R_iM4e.K9w1BvJa683lEz@ji:uF{' );
define( 'LOGGED_IN_SALT',   '@v&0H8:bA-XF6EJ@lwkJ$Kv211+PJ62{DpwZO4eCovjGV$Ywdv K`ijz~WMMdl?y' );
define( 'NONCE_SALT',       '@ApUXjW]JaM/rI+-B6u/6o96k%s`3~FPX6ce)2z+JG-*E&z8WXZ`:r237>t)1#X`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

